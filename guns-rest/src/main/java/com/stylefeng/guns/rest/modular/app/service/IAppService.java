package com.stylefeng.guns.rest.modular.app.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.rest.common.persistence.model.Detail;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 移动端 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-06-13
 */
public interface IAppService extends IService<Detail> {
}
