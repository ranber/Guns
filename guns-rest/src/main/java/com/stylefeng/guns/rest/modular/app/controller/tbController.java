package com.stylefeng.guns.rest.modular.app.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.rest.VO.ResultVO;
import com.stylefeng.guns.rest.common.persistence.model.Detail;
import com.stylefeng.guns.rest.modular.app.service.IAppService;
import com.stylefeng.guns.rest.modular.app.util.ResultVOUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @Auther: liaoshiquan
 * @Date: 2018/6/18 09:14
 * @Description:
 */

@RestController
@RequestMapping("/taobao")
@Slf4j
@Api(value ="taobao",description = "淘宝数据相关的接口")
public class tbController {

    @Autowired
    IAppService iAppService;

    /**
     * 通过搜索词获取优惠券数据
     *
     * @param name 搜索词
     * @return
     */
    @ApiOperation(value = "获取优惠券信息", notes = "根据name关键字来匹配数据库数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "APP端搜索关键词",defaultValue = "女包",required = true, dataType = "String",paramType = "query"),
            @ApiImplicitParam(name = "currentPage", value = "当前显示页码", dataType = "Integer",paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", dataType = "Integer",paramType = "query")
    })
    @PostMapping("/search")
    public ResultVO searchByName(@RequestParam("name") String name,
                                 @RequestParam(value = "currentPage", defaultValue = "1") int currentPage,
                                 @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        Page<Detail> first_type = iAppService.selectPage(
                new Page<>(currentPage, pageSize),
                new EntityWrapper<Detail>().like("first_type", name)
        );
        return ResultVOUtil.success(first_type);
    }
}
