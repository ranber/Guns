package com.stylefeng.guns.rest.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.rest.common.persistence.model.Detail;

/**
 * <p>
 * 淘宝推广详情表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-06-13
 */
public interface DetailMapper extends BaseMapper<Detail> {

}
