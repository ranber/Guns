package com.stylefeng.guns.modular.taobao.service;

import com.stylefeng.guns.modular.system.model.Detail;
import com.baomidou.mybatisplus.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 淘宝推广详情表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-06-13
 */
public interface IDetailService extends IService<Detail> {

    Boolean batchImport(String saveFilePath,String fileName, MultipartFile file);
}
