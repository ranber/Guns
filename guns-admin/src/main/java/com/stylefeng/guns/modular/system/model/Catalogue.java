package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * app轮播目录表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-06-19
 */
@TableName("app_catalogue")
public class Catalogue extends Model<Catalogue> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 目录名字
     */
    @TableField("catalogue_name")
    private String catalogueName;
    /**
     * 轮播图片地址
     */
    @TableField("img_url")
    private String imgUrl;
    /**
     * 跳转链接url
     */
    private String url;
    /**
     * url类型,0:H5,1:内部
     */
    @TableField("url_type")
    private Integer urlType;
    /**
     * 目录对应位置
     */
    @TableField("catalogue_index")
    private Integer catalogueIndex;
    /**
     * 0:轮播,1:条目
     */
    @TableField("data_type")
    private Integer dataType;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCatalogueName() {
        return catalogueName;
    }

    public void setCatalogueName(String catalogueName) {
        this.catalogueName = catalogueName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getUrlType() {
        return urlType;
    }

    public void setUrlType(Integer urlType) {
        this.urlType = urlType;
    }

    public Integer getCatalogueIndex() {
        return catalogueIndex;
    }

    public void setCatalogueIndex(Integer catalogueIndex) {
        this.catalogueIndex = catalogueIndex;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Catalogue{" +
        "id=" + id +
        ", catalogueName=" + catalogueName +
        ", imgUrl=" + imgUrl +
        ", url=" + url +
        ", urlType=" + urlType +
        ", catalogueIndex=" + catalogueIndex +
        ", dataType=" + dataType +
        "}";
    }
}
