package com.stylefeng.guns.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 淘宝推广详情表
 * </p>
 *
 * @author stylefeng123
 * @since 2018-06-13
 */
@TableName("taobao_detail")
public class Detail extends Model<Detail> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 商品id
     */
    @TableField("product_id")
    private String productId;
    /**
     * 商品名称
     */
    @TableField("product_name")
    private String productName;
    /**
     * 商品价格
     */
    @TableField("product_price")
    private Float productPrice;
    /**
     * 商品一级类目
     */
    @TableField("first_type")
    private String firstType;
    /**
     * 券后价
     */
    @TableField("ticketd_price")
    private Float ticketdPrice;
    /**
     * 优惠券面额
     */
    @TableField("ticket_denomination")
    private String ticketDenomination;
    /**
     * 平台类型,天猫,淘宝
     */
    @TableField("platform_type")
    private String platformType;
    /**
     * 店铺名称
     */
    @TableField("name_of_shop")
    private String nameOfShop;
    /**
     * 商品链接
     */
    @TableField("priduct_url")
    private String priductUrl;
    /**
     * 商品主图
     */
    @TableField("priduct_image")
    private String priductImage;
    /**
     * 收入比率
     */
    @TableField("income_ratio")
    private Float incomeRatio;
    /**
     * 开推时间
     */
    @TableField("activity_time_start")
    private Date activityTimeStart;
    /**
     * 优惠券开始时间
     */
    @TableField("ticket_time_start")
    private Date ticketTimeStart;
    /**
     * 优惠券结束时间
     */
    @TableField("ticket_time_end")
    private Date ticketTimeEnd;
    /**
     * 优惠券总量
     */
    @TableField("ticket_amount")
    private Integer ticketAmount;
    /**
     * 优惠券剩余量
     */
    @TableField("ticket_residue")
    private Integer ticketResidue;
    /**
     * 优惠券id
     */
    @TableField("ticket_id")
    private String ticketId;
    /**
     * 优惠券推广链接
     */
    @TableField("ticket_generalize_url")
    private String ticketGeneralizeUrl;
    /**
     * 推广链接
     */
    @TableField("referral_links")
    private String referralLinks;
    /**
     * 官方推荐
     */
    private String recommend;
    /**
     * 商品详情页
     */
    @TableField("product_detail_page")
    private String productDetailPage;
    /**
     * 淘宝客链接
     */
    @TableField("taobao_guest_url")
    private String taobaoGuestUrl;
    /**
     * 月销量
     */
    @TableField("product_monthly_sales")
    private Integer productMonthlySales;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Float productPrice) {
        this.productPrice = productPrice;
    }

    public String getFirstType() {
        return firstType;
    }

    public void setFirstType(String firstType) {
        this.firstType = firstType;
    }

    public Float getTicketdPrice() {
        return ticketdPrice;
    }

    public void setTicketdPrice(Float ticketdPrice) {
        this.ticketdPrice = ticketdPrice;
    }

    public String getTicketDenomination() {
        return ticketDenomination;
    }

    public void setTicketDenomination(String ticketDenomination) {
        this.ticketDenomination = ticketDenomination;
    }

    public String getPlatformType() {
        return platformType;
    }

    public void setPlatformType(String platformType) {
        this.platformType = platformType;
    }

    public String getNameOfShop() {
        return nameOfShop;
    }

    public void setNameOfShop(String nameOfShop) {
        this.nameOfShop = nameOfShop;
    }

    public String getPriductUrl() {
        return priductUrl;
    }

    public void setPriductUrl(String priductUrl) {
        this.priductUrl = priductUrl;
    }

    public String getPriductImage() {
        return priductImage;
    }

    public void setPriductImage(String priductImage) {
        this.priductImage = priductImage;
    }

    public Float getIncomeRatio() {
        return incomeRatio;
    }

    public void setIncomeRatio(Float incomeRatio) {
        this.incomeRatio = incomeRatio;
    }

    public Date getActivityTimeStart() {
        return activityTimeStart;
    }

    public void setActivityTimeStart(Date activityTimeStart) {
        this.activityTimeStart = activityTimeStart;
    }

    public Date getTicketTimeStart() {
        return ticketTimeStart;
    }

    public void setTicketTimeStart(Date ticketTimeStart) {
        this.ticketTimeStart = ticketTimeStart;
    }

    public Date getTicketTimeEnd() {
        return ticketTimeEnd;
    }

    public void setTicketTimeEnd(Date ticketTimeEnd) {
        this.ticketTimeEnd = ticketTimeEnd;
    }

    public Integer getTicketAmount() {
        return ticketAmount;
    }

    public void setTicketAmount(Integer ticketAmount) {
        this.ticketAmount = ticketAmount;
    }

    public Integer getTicketResidue() {
        return ticketResidue;
    }

    public void setTicketResidue(Integer ticketResidue) {
        this.ticketResidue = ticketResidue;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getTicketGeneralizeUrl() {
        return ticketGeneralizeUrl;
    }

    public void setTicketGeneralizeUrl(String ticketGeneralizeUrl) {
        this.ticketGeneralizeUrl = ticketGeneralizeUrl;
    }

    public String getReferralLinks() {
        return referralLinks;
    }

    public void setReferralLinks(String referralLinks) {
        this.referralLinks = referralLinks;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    public String getProductDetailPage() {
        return productDetailPage;
    }

    public void setProductDetailPage(String productDetailPage) {
        this.productDetailPage = productDetailPage;
    }

    public String getTaobaoGuestUrl() {
        return taobaoGuestUrl;
    }

    public void setTaobaoGuestUrl(String taobaoGuestUrl) {
        this.taobaoGuestUrl = taobaoGuestUrl;
    }

    public Integer getProductMonthlySales() {
        return productMonthlySales;
    }

    public void setProductMonthlySales(Integer productMonthlySales) {
        this.productMonthlySales = productMonthlySales;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Detail{" +
        "id=" + id +
        ", productId=" + productId +
        ", productName=" + productName +
        ", productPrice=" + productPrice +
        ", firstType=" + firstType +
        ", ticketdPrice=" + ticketdPrice +
        ", ticketDenomination=" + ticketDenomination +
        ", platformType=" + platformType +
        ", nameOfShop=" + nameOfShop +
        ", priductUrl=" + priductUrl +
        ", priductImage=" + priductImage +
        ", incomeRatio=" + incomeRatio +
        ", activityTimeStart=" + activityTimeStart +
        ", ticketTimeStart=" + ticketTimeStart +
        ", ticketTimeEnd=" + ticketTimeEnd +
        ", ticketAmount=" + ticketAmount +
        ", ticketResidue=" + ticketResidue +
        ", ticketId=" + ticketId +
        ", ticketGeneralizeUrl=" + ticketGeneralizeUrl +
        ", referralLinks=" + referralLinks +
        ", recommend=" + recommend +
        ", productDetailPage=" + productDetailPage +
        ", taobaoGuestUrl=" + taobaoGuestUrl +
        ", productMonthlySales=" + productMonthlySales +
        "}";
    }
}
