package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.Catalogue;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * app轮播目录表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-06-19
 */
public interface CatalogueMapper extends BaseMapper<Catalogue> {

}
