package com.stylefeng.guns.modular.app.service;

import com.stylefeng.guns.modular.system.model.Catalogue;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * app轮播目录表 服务类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-06-19
 */
public interface ICatalogueService extends IService<Catalogue> {

}
