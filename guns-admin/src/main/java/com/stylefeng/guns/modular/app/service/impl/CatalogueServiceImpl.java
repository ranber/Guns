package com.stylefeng.guns.modular.app.service.impl;

import com.stylefeng.guns.modular.system.model.Catalogue;
import com.stylefeng.guns.modular.system.dao.CatalogueMapper;
import com.stylefeng.guns.modular.app.service.ICatalogueService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * app轮播目录表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-06-19
 */
@Service
public class CatalogueServiceImpl extends ServiceImpl<CatalogueMapper, Catalogue> implements ICatalogueService {

}
