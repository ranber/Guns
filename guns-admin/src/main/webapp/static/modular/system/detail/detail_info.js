/**
 * 初始化taobao详情对话框
 */
var DetailInfoDlg = {
    detailInfoData : {}
};

/**
 * 清除数据
 */
DetailInfoDlg.clearData = function() {
    this.detailInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
DetailInfoDlg.set = function(key, val) {
    this.detailInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
DetailInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
DetailInfoDlg.close = function() {
    parent.layer.close(window.parent.Detail.layerIndex);
}

/**
 * 收集数据
 */
DetailInfoDlg.collectData = function() {
    this
    .set('id')
    .set('productId')
    .set('productName')
    .set('productPrice')
    .set('firstType')
    .set('ticketdPrice')
    .set('ticketDenomination')
    .set('platformType')
    .set('nameOfShop')
    .set('priductUrl')
    .set('priductImage')
    .set('incomeRatio')
    .set('activityTimeStart')
    .set('ticketTimeStart')
    .set('ticketTimeEnd')
    .set('ticketAmount')
    .set('ticketResidue')
    .set('ticketId')
    .set('ticketGeneralizeUrl')
    .set('referralLinks')
    .set('recommend')
    .set('productDetailPage')
    .set('taobaoGuestUrl')
    .set('productMonthlySales');
}

/**
 * 提交添加
 */
DetailInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/detail/add", function(data){
        Feng.success("添加成功!");
        window.parent.Detail.table.refresh();
        DetailInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.detailInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
DetailInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/detail/update", function(data){
        Feng.success("修改成功!");
        window.parent.Detail.table.refresh();
        DetailInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.detailInfoData);
    ajax.start();
}


$(function() {

});
