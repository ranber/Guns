/**
 * app首页配置管理初始化
 */
var Catalogue = {
    id: "CatalogueTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Catalogue.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '目录名字', field: 'catalogueName', visible: true, align: 'center', valign: 'middle'},
            {title: '轮播图片地址', field: 'imgUrl', visible: true, align: 'center', valign: 'middle'},
            {title: '跳转链接url', field: 'url', visible: true, align: 'center', valign: 'middle'},
            {title: 'url类型,0:H5,1:内部', field: 'urlType', visible: true, align: 'center', valign: 'middle'},
            {title: '目录对应位置', field: 'catalogueIndex', visible: true, align: 'center', valign: 'middle'},
            {title: '0:轮播,1:条目', field: 'dataType', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Catalogue.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Catalogue.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加app首页配置
 */
Catalogue.openAddCatalogue = function () {
    var index = layer.open({
        type: 2,
        title: '添加app首页配置',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/catalogue/catalogue_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看app首页配置详情
 */
Catalogue.openCatalogueDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: 'app首页配置详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/catalogue/catalogue_update/' + Catalogue.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除app首页配置
 */
Catalogue.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/catalogue/delete", function (data) {
            Feng.success("删除成功!");
            Catalogue.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("catalogueId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询app首页配置列表
 */
Catalogue.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Catalogue.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Catalogue.initColumn();
    var table = new BSTable(Catalogue.id, "/catalogue/list", defaultColunms);
    table.setPaginationType("client");
    Catalogue.table = table.init();
});
