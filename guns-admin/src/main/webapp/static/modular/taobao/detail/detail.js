/**
 * 淘宝券详情管理初始化
 */
var Detail = {
    id: "DetailTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Detail.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '主键id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '商品id', field: 'productId', visible: true, align: 'center', valign: 'middle'},
            {title: '商品名称', field: 'productName', visible: true, align: 'center', valign: 'middle'},
            {title: '商品价格', field: 'productPrice', visible: true, align: 'center', valign: 'middle'},
            {title: '商品一级类目', field: 'firstType', visible: true, align: 'center', valign: 'middle'},
            {title: '券后价', field: 'ticketdPrice', visible: true, align: 'center', valign: 'middle'},
            {title: '优惠券面额', field: 'ticketDenomination', visible: true, align: 'center', valign: 'middle'},
            {title: '平台类型,天猫,淘宝', field: 'platformType', visible: true, align: 'center', valign: 'middle'},
            {title: '店铺名称', field: 'nameOfShop', visible: true, align: 'center', valign: 'middle'},
            {title: '商品链接', field: 'priductUrl', visible: true, align: 'center', valign: 'middle'},
            {title: '商品主图', field: 'priductImage', visible: true, align: 'center', valign: 'middle'},
            {title: '收入比率', field: 'incomeRatio', visible: true, align: 'center', valign: 'middle'},
            {title: '开推时间', field: 'activityTimeStart', visible: true, align: 'center', valign: 'middle'},
            {title: '优惠券开始时间', field: 'ticketTimeStart', visible: true, align: 'center', valign: 'middle'},
            {title: '优惠券结束时间', field: 'ticketTimeEnd', visible: true, align: 'center', valign: 'middle'},
            {title: '优惠券总量', field: 'ticketAmount', visible: true, align: 'center', valign: 'middle'},
            {title: '优惠券剩余量', field: 'ticketResidue', visible: true, align: 'center', valign: 'middle'},
            {title: '优惠券id', field: 'ticketId', visible: true, align: 'center', valign: 'middle'},
            {title: '优惠券推广链接', field: 'ticketGeneralizeUrl', visible: true, align: 'center', valign: 'middle'},
            {title: '推广链接', field: 'referralLinks', visible: true, align: 'center', valign: 'middle'},
            {title: '官方推荐', field: 'recommend', visible: true, align: 'center', valign: 'middle'},
            {title: '商品详情页', field: 'productDetailPage', visible: true, align: 'center', valign: 'middle'},
            {title: '淘宝客链接', field: 'taobaoGuestUrl', visible: true, align: 'center', valign: 'middle'},
            {title: '月销量', field: 'productMonthlySales', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Detail.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Detail.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加淘宝券详情
 */
Detail.openAddDetail = function () {
    var index = layer.open({
        type: 2,
        title: '添加淘宝券详情',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/detail/detail_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看淘宝券详情详情
 */
Detail.openDetailDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '淘宝券详情详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/detail/detail_update/' + Detail.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除淘宝券详情
 */
Detail.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/detail/delete", function (data) {
            Feng.success("删除成功!");
            Detail.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("detailId",this.seItem.id);
        ajax.start();
    }
};


/**
 * 批量添加数据
 */
Detail.upload = function () {
    var index = layer.open({
        type: 2,
        title: '批量添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/detail/detail_upload'
    });
    this.layerIndex = index;
};


/**
 * 查询淘宝券详情列表
 */
Detail.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Detail.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Detail.initColumn();
    var table = new BSTable(Detail.id, "/detail/list", defaultColunms);
    table.setPaginationType("client");
    Detail.table = table.init();
});
